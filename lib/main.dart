import 'dart:math';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:charts_flutter/flutter.dart' as charts;

var _spc = 1;
var _sc = 0;
var _firstTime = true;

List<charts.Color> _colorAdder() {
  List<charts.Color> _return = new List<charts.Color>();
  _return.add(charts.MaterialPalette.blue.shadeDefault);
  _return.add(charts.MaterialPalette.green.shadeDefault);
  _return.add(charts.MaterialPalette.red.shadeDefault);
  _return.add(charts.MaterialPalette.indigo.shadeDefault);
  _return.add(charts.MaterialPalette.teal.shadeDefault);
  _return.add(charts.MaterialPalette.cyan.shadeDefault);
  _return.add(charts.MaterialPalette.deepOrange.shadeDefault);
  _return.add(charts.MaterialPalette.pink.shadeDefault);
  _return.add(charts.MaterialPalette.purple.shadeDefault);
  _return.add(charts.MaterialPalette.yellow.shadeDefault);

  return _return;
}

List<charts.Color> _colors = _colorAdder();
Random _rng = new Random();

List<List<Walker>> _createData() {
  List<List<Walker>> _walkers = new List<List<Walker>>();

  for (int i = 0; i < 10; i++) {
    List<Walker> _walkerList = new List<Walker>();
    for (int j = 0; j < 101; j++) {
      int _rnd = _rng.nextInt(2) == 0 ? -1 : 1;
      Walker _walker = _walkerList.length == 0
          ? new Walker(j, 0)
          : new Walker(j, _walkerList[j - 1].position + _rnd);
      _walkerList.add(_walker);
    }
    _walkers.add(_walkerList);
  }
  return _walkers;
}

List<List<Walker>> _walkersData = _createData();

List<charts.Series<Walker, int>> _createSeriesData() {
  List<List<Walker>> _walkers = new List<List<Walker>>();

  for (int i = 0; i < _spc; i++) {
    List<Walker> _walkerList = new List<Walker>();
    for (int j = 0; j <= _sc; j++) {
      _walkerList.add(_walkersData[i][j]);
    }
    _walkers.add(_walkerList);
  }

  List<charts.Series<Walker, int>> _return =
      new List<charts.Series<Walker, int>>();

  for (int i = 0; i < _spc; i++) {
    charts.Series<Walker, int> _value = new charts.Series<Walker, int>(
      id: '$i',
      colorFn: (_, __) => _colors[i],
      domainFn: (Walker walker, _) => walker.step,
      measureFn: (Walker walker, _) => walker.position,
      data: _walkers[i],
    );
    _return.add(_value);
  }

  return _return;
}

List<charts.Series<Walker, int>> _seriesData = _createSeriesData();

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Random Walker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Random Walker Project'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      ///for now {AppBar} is not needed!
//      appBar: AppBar(
//        title: Text(widget.title),
//        centerTitle: true,
//      ),
      body: Row(
        children: <Widget>[
          Expanded(
            child: SafeArea(
              child: DashPatternLineChart(
                _seriesData,
                animate: true,
              ),
            ),
          ),
          OrientationBuilder(
            builder: (context, orientation) {
              Future.delayed(Duration(milliseconds: 500), () {
                if (!_firstTime) return;
                _firstTime = false;
                setState(() {});
              });

              return Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('sample path count'),
                      NumberPicker.integer(
                        initialValue: _spc,
                        minValue: 1,
                        maxValue: 10,
                        onChanged: (num _num) {
                          setState(() {
                            _spc = _num;
                            _seriesData = _createSeriesData();
                          });
                        },
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('step count'),
                      NumberPicker.integer(
                        initialValue: _sc,
                        minValue: 0,
                        maxValue: 100,
                        onChanged: (num _num) {
                          setState(() {
                            _sc = _num;
                            _seriesData = _createSeriesData();
                          });
                        },
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}

class Walker {
  final int step;
  final int position;

  Walker(this.step, this.position);
}

class DashPatternLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DashPatternLineChart(this.seriesList, {this.animate});

  @override
  Widget build(BuildContext context) {
    return charts.LineChart(seriesList, animate: animate);
  }
}
